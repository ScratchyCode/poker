// Coded by Pietro Squilla aka ScratchyCode aka 0stile
/*
    *************************************
    ******** Poker Texas Hold'em ********
    *************************************

            ***    INFO    ***
    Le carte sono distribuite su una matrice, ordinate in base ai semi:
        prima riga:     cuori
        seconda riga:   quadri
        terza riga:     fiori
        quarta riga:    picche
    
    I valori delle carte sono indicati dall'indice dell'array, dove 0 è l'asso e 12 il K
    La matrice è inizializzata a 1, questo perchè ogni carta è nel mazzo.
    Quando una carta viene pescata la sua casella viene impostata a 0, per indicare la sua assenza nel mazzo.
    
DUBBIO:
Probabilità p di estrarre una carta utile dopo n estrazioni precedenti in una fase generica del gioco:
con P che è l'ipergeometrica e k il numero di out:
    for i=1 to n: sum( (P(k=i)/(coeff.bin(n i)) * coeff.bin(n k-1)) )

CERTO:
Per avere le probabilità di chiudere un punto dopo il flop bisogna stimare le carte outs necessarie (cioè di carte buone per fare un punto); fatto questo bisogna stimare il numero atteso delle outs nel mazzo dopo n estrazioni dal mazzo usando la distribuzione di probabilità ipergeometrica e dividerlo per il numero di carte presenti nel mazzo (dipendente dal numero di giocatori).

Per ottenere il valore atteso delle outs: se le outs totali sono la somma di più tipi di carte outs bisogna o modificare il parametro dei successi possibili nell'ipergeometrica, o equivalentemente il valore atteso delle outs totali è la somma dei valori attesi dei singoli tipi di carte outs per le proprietà dell'ipergeometrica.

ALTERNATIVA:
Per stimare in media il numero di carte che vengono tolte dopo il flop dai giocatori bisogna usare il valore atteso dell'ipergeometrica. Questo valore sarà il numero di out in meno. Quindi la probabilità di pescare una out dopo il flop sarà:
    (out - val.atteso) / numero carte nel mazzo

*/
/*
DA FINIRE:
    Funzione che stabilisce i punti fatti da ogni giocatore <-- importante per definizioni frequentiste dei punti
    Funzione che stabilisce il vincitore della mano.
    Funzione che valuta le percentuali dei punti --> probabilità di vincita in base alle carte sul tavolo.
    Funzione che, assegnata una probabilità entro un certo range (0,0.3), "decide" di bluffare casualmente.
    Funzione che valuta se vale la pena puntare (una cifra adeguatamente calcolata in base alle probabilità di vincita),
    vedere o rilanciare agli altri giocatori in base alle proprie probabilità di vincita o in base al bluff portato avanti.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define MAX_LEN 512
#define MIN_BUDGET 50
#define MAX_BUDGET 1000000
#define MIN_SB 1
#define MAX_SB 10000
#define NUM_TAVOLI 1

/****************************** STRUTTURE DATI DI GIOCO ******************************/
struct giocatore{
    // budget del giocatore
    double budget;
    
    /**** azioni ****/
    double bet;
    double raise;
    int fold;
    int check;
    int call;
    /***** azioni *****/
    
    /***** punti *****/
    int kicker;
    int cartaAlta;
    int coppia;
    int doppiaCoppia;
    int tris;
    int scala;
    int colore;
    int full;
    int poker;
    int scalaColore;
    int scalaReale;
    /***** punti *****/
    
    // coordinate prima carta
    int carta1R, carta1C;
    // coordinate seconda carta
    int carta2R, carta2C;
    
    // info carte
    int valoreCarta1, valoreCarta2;
    char semeCarta1, semeCarta2;
    
    // nome giocatore
    char nome[MAX_LEN];
};

struct carteComuni{
    // coordinate carte
    int cartaComune1R, cartaComune1C;
    int cartaComune2R, cartaComune2C;
    int cartaComune3R, cartaComune3C;
    int cartaComune4R, cartaComune4C;
    int cartaComune5R, cartaComune5C;
    
    // info carte
    int valoreCarta1, valoreCarta2, valoreCarta3, valoreCarta4, valoreCarta5;
    char semeCarta1, semeCarta2, semeCarta3, semeCarta4, semeCarta5;
    
    // nome vincitore
    char vincitore[MAX_LEN];
};
/****************************** STRUTTURE DATI DI GIOCO ******************************/

/****************************** DEFINIZIONI DI FUNZIONI ******************************/
void initGioco(int numGiocatori, struct giocatore infoGiocatore[], double budget);
void initPunti(int i, struct giocatore infoGiocatore[]);
void mescola();
void distribuisciCartePrivate(int numGiocatori, struct giocatore infoGiocatore[], int **mazzo);
void leggiCartePrivate(int i, struct giocatore infoGiocatore[]);
void flopTurnRiver(int i, struct carteComuni tavolo[], int **mazzo);
void leggiTavolo(int i, struct carteComuni tavolo[]);
void puntiGiocatori(int numGiocatori, struct giocatore infoGiocatore[], struct carteComuni tavolo[]);
void vincitore(int numGiocatori, struct giocatore infoGiocatore[], struct carteComuni tavolo[]);

double input(double min, double max);
int realRand(int min, int max);
int **creaMatrice(int righe, int colonne);
void initMatrice(int righe, int colonne, int **M, int valore);
void liberaMatrice(int righe, int **M);
void merge(int *A, int *L, int leftCount, int *R, int rightCount);
void mergeSort(int *A, int n);
/****************************** DEFINIZIONI DI FUNZIONI ******************************/

/*************************************** MAIN ****************************************/
int main(){
    int i, j, numGiocatori, scarto;
    double budget, piccoloBuio, grandeBuio;
    
    printf("Inserisci il numero di giocatori: ");
    numGiocatori = (int)input(2,10);
    
    printf("Inserisci il budget iniziale dei giocatori: ");
    budget = input(MIN_BUDGET,MAX_BUDGET);
    
    printf("Inserisci il piccolo buio: ");
    piccoloBuio = input(MIN_SB,MAX_SB);
    if(piccoloBuio > (0.25 * budget)){
        printf("\nErrore: il piccolo buio deve essere al massimo un quarto del budget iniziale!\n");
        exit(0);
    }
    grandeBuio = piccoloBuio * 2;
    
    // crea mazzo di carte allocando una matrice inizializzata ad 1
    int **mazzo = creaMatrice(4,13);
    initMatrice(4,13,mazzo,1);
    
    // le info sui giocatori in un array di strutture
    struct giocatore infoGiocatore[numGiocatori];
    // le info sulle carte a tavola in una struttura
    struct carteComuni tavolo[NUM_TAVOLI];
    
    // inizializza i valori della struttura giocatore
    initGioco(numGiocatori,infoGiocatore,budget);
    
    // mescola il mazzo (inizializzo la sequenza pseudocasuale)
    mescola();
    
    // assegna le carte ai giocatori (e tutte le altre info)
    distribuisciCartePrivate(numGiocatori,infoGiocatore,mazzo);
    
    /* ...puntate... */
    
    // metti a tavola le carte
    flopTurnRiver(0,tavolo,mazzo);
    
    // leggi le carte che sono a tavola
    leggiTavolo(0,tavolo);
    
    // mostra le carte a tavola
    printf("\nA tavola c'è:\n%d%c\t%d%c\t%d%c\t%d%c\t%d%c\n",
    tavolo[0].valoreCarta1,tavolo[0].semeCarta1,
    tavolo[0].valoreCarta2,tavolo[0].semeCarta2,
    tavolo[0].valoreCarta3,tavolo[0].semeCarta3,
    tavolo[0].valoreCarta4,tavolo[0].semeCarta4,
    tavolo[0].valoreCarta5,tavolo[0].semeCarta5);
    
    // mostra le carte dei giocatori
    for(i=0; i<numGiocatori; i++){
        printf("\n%s ha: %d%c e %d%c",infoGiocatore[i].nome,
        infoGiocatore[i].valoreCarta1,infoGiocatore[i].semeCarta1,
        infoGiocatore[i].valoreCarta2,infoGiocatore[i].semeCarta2);
    }
    
    // determina i punti dei giocatori
    puntiGiocatori(numGiocatori,infoGiocatore,tavolo);
    
    // determina il punto più alto tra quelli dei giocatori
    vincitore(numGiocatori,infoGiocatore,tavolo);
    
    // dealloca la memoria usata per memorizzare il mazzo
    liberaMatrice(4,mazzo);
    
    printf("\n\n");
    return 0;
}
/*************************************** MAIN ****************************************/

/************************************* FUNZIONI **************************************/
void initGioco(int numGiocatori, struct giocatore infoGiocatore[], double budget){
    int i;
    
    getchar();
    for(i=0; i<numGiocatori; i++){
        // nome del giocatore
        printf("Inserisci il nome del giocatore %d: ",i+1);
        fgets(infoGiocatore[i].nome,MAX_LEN,stdin);
        infoGiocatore[i].nome[strlen(infoGiocatore[i].nome) - 1] = '\0';
        
        // assegna il budget iniziale
        infoGiocatore[i].budget = budget;
        
        // inizializza le flag di gioco
        initPunti(i,infoGiocatore);
    }
    
    return ;
}

void initPunti(int i, struct giocatore infoGiocatore[]){
    /***** azioni *****/
    infoGiocatore[i].bet = 0;
    infoGiocatore[i].raise = 0;
    infoGiocatore[i].fold = 0;
    infoGiocatore[i].check = 0;
    infoGiocatore[i].call = 0;
    /***** azioni *****/
    
    /***** punti *****/
    infoGiocatore[i].cartaAlta = 0;
    infoGiocatore[i].coppia = 0;
    infoGiocatore[i].doppiaCoppia = 0;
    infoGiocatore[i].tris = 0;
    infoGiocatore[i].scala = 0;
    infoGiocatore[i].colore = 0;
    infoGiocatore[i].full = 0;
    infoGiocatore[i].poker = 0;
    infoGiocatore[i].scalaColore = 0;
    infoGiocatore[i].scalaReale = 0;
    /***** punti *****/
    
    return ;
}

void mescola(){
    srand48(time(NULL));
}

void distribuisciCartePrivate(int numGiocatori, struct giocatore infoGiocatore[], int **mazzo){
    int i;
    
    for(i=0; i<numGiocatori; i++){
        // distribuisce la prima carta
        do{
            infoGiocatore[i].carta1R = realRand(0,4);
            infoGiocatore[i].carta1C = realRand(0,13);
            // se la carta supera la grandezza della matrice di 1
            if(infoGiocatore[i].carta1R == 4){
                infoGiocatore[i].carta1R = 3;
            }
            if(infoGiocatore[i].carta1C == 13){
                infoGiocatore[i].carta1R = 12;
            }
        }while(mazzo[infoGiocatore[i].carta1R][infoGiocatore[i].carta1C] == 0);
        // toglie dal mazzo la prima carta
        mazzo[infoGiocatore[i].carta1R][infoGiocatore[i].carta1C] = 0;
        
        // distribuisce la seconda carta
        do{
            infoGiocatore[i].carta2R = realRand(0,4);
            infoGiocatore[i].carta2C = realRand(0,13);
            // se la carta supera la grandezza della matrice di 1
            if(infoGiocatore[i].carta2R == 4){
                infoGiocatore[i].carta2R = 3;
            }
            if(infoGiocatore[i].carta2C == 13){
                infoGiocatore[i].carta2R = 12;
            }
        }while(mazzo[infoGiocatore[i].carta2R][infoGiocatore[i].carta2C] == 0);
        // toglie dal mazzo la seconda carta
        mazzo[infoGiocatore[i].carta2R][infoGiocatore[i].carta2C] = 0;
        
        // memorizza il tipo di carta posseduta
        leggiCartePrivate(i,infoGiocatore);
    }
    
    return ;
}

void leggiCartePrivate(int i, struct giocatore infoGiocatore[]){
    // analizzo prima carta
    // seme
    if(infoGiocatore[i].carta1R == 0){
        infoGiocatore[i].semeCarta1 = 'C';
    }else if(infoGiocatore[i].carta1R == 1){
        infoGiocatore[i].semeCarta1 = 'Q';
    }else if(infoGiocatore[i].carta1R == 2){
        infoGiocatore[i].semeCarta1 = 'F';
    }else if(infoGiocatore[i].carta1R == 3){
        infoGiocatore[i].semeCarta1 = 'P';
    }
    //valore
    infoGiocatore[i].valoreCarta1 = infoGiocatore[i].carta1C + 1;
    
    // analizzo seconda carta
    // seme
    if(infoGiocatore[i].carta2R == 0){
        infoGiocatore[i].semeCarta2 = 'C';
    }else if(infoGiocatore[i].carta2R == 1){
        infoGiocatore[i].semeCarta2 = 'Q';
    }else if(infoGiocatore[i].carta2R == 2){
        infoGiocatore[i].semeCarta2 = 'F';
    }else if(infoGiocatore[i].carta2R == 3){
        infoGiocatore[i].semeCarta2 = 'P';
    }
    //valore
    infoGiocatore[i].valoreCarta2 = infoGiocatore[i].carta2C + 1;
    
    return ;
}

void flopTurnRiver(int i, struct carteComuni tavolo[], int **mazzo){
    // flop (prima carta da scartare)
    do{
        tavolo[i].cartaComune1R = realRand(0,4);
        tavolo[i].cartaComune1C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune1R == 4){
            tavolo[i].cartaComune1R = 3;
        }
        if(tavolo[i].cartaComune1C == 13){
            tavolo[i].cartaComune1C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune1R][tavolo[i].cartaComune1C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune1R][tavolo[i].cartaComune1C] = 0;
    
    // prima carta del flop (rimane a tavolo)
    do{
        tavolo[i].cartaComune1R = realRand(0,4);
        tavolo[i].cartaComune1C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune1R == 4){
            tavolo[i].cartaComune1R = 3;
        }
        if(tavolo[i].cartaComune1C == 13){
            tavolo[i].cartaComune1C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune1R][tavolo[i].cartaComune1C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune1R][tavolo[i].cartaComune1C] = 0;
    
    // seconda carta del flop (rimane a tavolo)
    do{
        tavolo[i].cartaComune2R = realRand(0,4);
        tavolo[i].cartaComune2C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune2R == 4){
            tavolo[i].cartaComune2R = 3;
        }
        if(tavolo[i].cartaComune2C == 13){
            tavolo[i].cartaComune2C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune2R][tavolo[i].cartaComune2C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune2R][tavolo[i].cartaComune2C] = 0;
    
    // terza carta del flop (rimane a tavolo)
    do{
        tavolo[i].cartaComune3R = realRand(0,4);
        tavolo[i].cartaComune3C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune3R == 4){
            tavolo[i].cartaComune3R = 3;
        }
        if(tavolo[i].cartaComune3C == 13){
            tavolo[i].cartaComune3C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune3R][tavolo[i].cartaComune3C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune3R][tavolo[i].cartaComune3C] = 0;
    
    // turn (la prima è da scartare)
    do{
        tavolo[i].cartaComune4R = realRand(0,4);
        tavolo[i].cartaComune4C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune4R == 4){
            tavolo[i].cartaComune4R = 3;
        }
        if(tavolo[i].cartaComune4C == 13){
            tavolo[i].cartaComune4C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune4R][tavolo[i].cartaComune4C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune4R][tavolo[i].cartaComune4C] = 0;
    
    // turn (rimane a tavolo)
    do{
        tavolo[i].cartaComune4R = realRand(0,4);
        tavolo[i].cartaComune4C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune4R == 4){
            tavolo[i].cartaComune4R = 3;
        }
        if(tavolo[i].cartaComune4C == 13){
            tavolo[i].cartaComune4C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune4R][tavolo[i].cartaComune4C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune4R][tavolo[i].cartaComune4C] = 0;
    
    // river (la prima è da scartare)
    do{
        tavolo[i].cartaComune5R = realRand(0,4);
        tavolo[i].cartaComune5C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune5R == 4){
            tavolo[i].cartaComune5R = 3;
        }
        if(tavolo[i].cartaComune5C == 13){
            tavolo[i].cartaComune5C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune5R][tavolo[i].cartaComune5C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune5R][tavolo[i].cartaComune5C] = 0;
    
    // river (rimane a tavolo)
    do{
        tavolo[i].cartaComune5R = realRand(0,4);
        tavolo[i].cartaComune5C = realRand(0,13);
        // se la carta supera la grandezza della matrice di 1
        if(tavolo[i].cartaComune5R == 4){
            tavolo[i].cartaComune5R = 3;
        }
        if(tavolo[i].cartaComune5C == 13){
            tavolo[i].cartaComune5C = 12;
        }
    }while(mazzo[tavolo[i].cartaComune5R][tavolo[i].cartaComune5C] == 0);
    // toglie dal mazzo la carta
    mazzo[tavolo[i].cartaComune5R][tavolo[i].cartaComune5C] = 0;
    
    return ;
}

void leggiTavolo(int i, struct carteComuni tavolo[]){
    // 1° carta
    // seme
    if(tavolo[i].cartaComune1R == 0){
        tavolo[i].semeCarta1 = 'C';
    }else if(tavolo[i].cartaComune1R == 1){
        tavolo[i].semeCarta1 = 'Q';
    }else if(tavolo[i].cartaComune1R == 2){
        tavolo[i].semeCarta1 = 'F';
    }else if(tavolo[i].cartaComune1R == 3){
        tavolo[i].semeCarta1 = 'P';
    }
    //valore
    tavolo[i].valoreCarta1 = tavolo[i].cartaComune1C + 1;
    
    // 2° carta
    // seme
    if(tavolo[i].cartaComune2R == 0){
        tavolo[i].semeCarta2 = 'C';
    }else if(tavolo[i].cartaComune2R == 1){
        tavolo[i].semeCarta2 = 'Q';
    }else if(tavolo[i].cartaComune2R == 2){
        tavolo[i].semeCarta2 = 'F';
    }else if(tavolo[i].cartaComune2R == 3){
        tavolo[i].semeCarta2 = 'P';
    }
    //valore
    tavolo[i].valoreCarta2 = tavolo[i].cartaComune2C + 1;
    
    // 3° carta
    // seme
    if(tavolo[i].cartaComune3R == 0){
        tavolo[i].semeCarta3 = 'C';
    }else if(tavolo[i].cartaComune3R == 1){
        tavolo[i].semeCarta3 = 'Q';
    }else if(tavolo[i].cartaComune3R == 2){
        tavolo[i].semeCarta3 = 'F';
    }else if(tavolo[i].cartaComune3R == 3){
        tavolo[i].semeCarta3 = 'P';
    }
    //valore
    tavolo[i].valoreCarta3 = tavolo[i].cartaComune3C + 1;
    
    // 4° carta
    // seme
    if(tavolo[i].cartaComune4R == 0){
        tavolo[i].semeCarta4 = 'C';
    }else if(tavolo[i].cartaComune4R == 1){
        tavolo[i].semeCarta4 = 'Q';
    }else if(tavolo[i].cartaComune4R == 2){
        tavolo[i].semeCarta4 = 'F';
    }else if(tavolo[i].cartaComune4R == 3){
        tavolo[i].semeCarta4 = 'P';
    }
    //valore
    tavolo[i].valoreCarta4 = tavolo[i].cartaComune4C + 1;
    
    // 5° carta
    // seme
    if(tavolo[i].cartaComune5R == 0){
        tavolo[i].semeCarta5 = 'C';
    }else if(tavolo[i].cartaComune5R == 1){
        tavolo[i].semeCarta5 = 'Q';
    }else if(tavolo[i].cartaComune5R == 2){
        tavolo[i].semeCarta5 = 'F';
    }else if(tavolo[i].cartaComune5R == 3){
        tavolo[i].semeCarta5 = 'P';
    }
    //valore
    tavolo[i].valoreCarta5 = tavolo[i].cartaComune5C + 1;
    
    return ;
}

void puntiGiocatori(int numGiocatori, struct giocatore infoGiocatore[], struct carteComuni tavolo[]){
    int i, tmp1=0, tmp2=0;
    
    for(i=0; i<numGiocatori; i++){
        // calcolo punti: coppia, doppia coppia, tris, poker (con la prima carta)
        if(infoGiocatore[i].valoreCarta1 == tavolo[0].valoreCarta1){
            tmp1++;
        }else if(infoGiocatore[i].valoreCarta1 == tavolo[0].valoreCarta2){
            tmp1++;
        }else if(infoGiocatore[i].valoreCarta1 == tavolo[0].valoreCarta3){
            tmp1++;
        }else if(infoGiocatore[i].valoreCarta1 == tavolo[0].valoreCarta4){
            tmp1++;
        }else if(infoGiocatore[i].valoreCarta1 == tavolo[0].valoreCarta5){
            tmp1++;
        }
        // analizzo
        if(tmp1 == 0){
            infoGiocatore[i].coppia = 0;
        }else if(tmp1 == 1){
            infoGiocatore[i].coppia = infoGiocatore[i].valoreCarta1;
        }else if(tmp1 == 2){
            infoGiocatore[i].tris = infoGiocatore[i].valoreCarta1;
        }else if(tmp1 == 4){
            infoGiocatore[i].poker = infoGiocatore[i].valoreCarta1;
        }
    
        // calcolo punti: coppia, doppia coppia, tris, poker (con la seconda carta)
        if(infoGiocatore[i].valoreCarta2 == tavolo[0].valoreCarta1){
            tmp2++;
        }else if(infoGiocatore[i].valoreCarta2 == tavolo[0].valoreCarta2){
            tmp2++;
        }else if(infoGiocatore[i].valoreCarta2 == tavolo[0].valoreCarta3){
            tmp2++;
        }else if(infoGiocatore[i].valoreCarta2 == tavolo[0].valoreCarta4){
            tmp2++;
        }else if(infoGiocatore[i].valoreCarta2 == tavolo[0].valoreCarta5){
            tmp2++;
        }
        // analizzo
        if(tmp2 == 0 && infoGiocatore[i].coppia == 0){
            infoGiocatore[i].coppia = 0;
        }else if(tmp2 == 1 && infoGiocatore[i].coppia == 0){
            infoGiocatore[i].coppia = infoGiocatore[i].valoreCarta2;
        }else if(tmp2 == 1 && infoGiocatore[i].coppia > 0){
            if(infoGiocatore[i].valoreCarta1 > infoGiocatore[i].valoreCarta2){
                infoGiocatore[i].doppiaCoppia = infoGiocatore[i].valoreCarta1;
            }else{
                infoGiocatore[i].doppiaCoppia = infoGiocatore[i].valoreCarta2;
            }
        }else if(tmp2 == 2 && infoGiocatore[i].coppia == 0){
            infoGiocatore[i].tris = infoGiocatore[i].valoreCarta2;
        }else if(tmp2 == 2 && infoGiocatore[i].coppia > 0){
            infoGiocatore[i].poker = infoGiocatore[i].valoreCarta2;
        }else if(tmp2 == 3){
            infoGiocatore[i].poker = infoGiocatore[i].valoreCarta2;
        }
        
        // calcolo punti: coppia, doppia coppia, tris, poker (carte private uguali)
        if(infoGiocatore[i].valoreCarta1 == infoGiocatore[i].valoreCarta2){
            infoGiocatore[i].coppia = infoGiocatore[i].valoreCarta1;
        }else if(infoGiocatore[i].valoreCarta1 == infoGiocatore[i].valoreCarta2 && tmp1 == 1){
            infoGiocatore[i].tris = infoGiocatore[i].valoreCarta1;
        }else if(infoGiocatore[i].valoreCarta1 == infoGiocatore[i].valoreCarta2 && tmp1 == 2){
            infoGiocatore[i].poker = infoGiocatore[i].valoreCarta1;
        }
    
        // calcolo punti: colore (prima carta)
        tmp1 = tmp2 = 0;
        if(infoGiocatore[i].semeCarta1 == tavolo[0].semeCarta1){
            tmp1++;
        }else if(infoGiocatore[i].semeCarta1 == tavolo[0].semeCarta2){
            tmp1++;
        }else if(infoGiocatore[i].semeCarta1 == tavolo[0].semeCarta3){
            tmp1++;
        }else if(infoGiocatore[i].semeCarta1 == tavolo[0].semeCarta4){
            tmp1++;
        }else if(infoGiocatore[i].semeCarta1 == tavolo[0].semeCarta5){
            tmp1++;
        }
        // analizzo
        if(tmp1 >= 4){
            infoGiocatore[i].colore = infoGiocatore[i].valoreCarta1;
        }
    
        // calcolo punti: colore (seconda carta)
        if(infoGiocatore[i].semeCarta2 == tavolo[0].semeCarta1){
            tmp2++;
        }else if(infoGiocatore[i].semeCarta2 == tavolo[0].semeCarta2){
            tmp2++;
        }else if(infoGiocatore[i].semeCarta2 == tavolo[0].semeCarta3){
            tmp2++;
        }else if(infoGiocatore[i].semeCarta2 == tavolo[0].semeCarta4){
            tmp2++;
        }else if(infoGiocatore[i].semeCarta2 == tavolo[0].semeCarta5){
            tmp2++;
        }
        // analizzo
        if(tmp2 >= 4){
            infoGiocatore[i].colore = infoGiocatore[i].valoreCarta2;
        }
    
        // calcolo punti: full
        if(infoGiocatore[i].tris > 0 && infoGiocatore[i].coppia > 0){
            infoGiocatore[i].full = infoGiocatore[i].tris;
        }
        
        // calcolo punti: scala
        tmp1 = tmp2 = 0;
        int scala[7];
        
        scala[0] = infoGiocatore[i].valoreCarta1;
        scala[1] = infoGiocatore[i].valoreCarta2;
        scala[2] = tavolo[0].valoreCarta1;
        scala[3] = tavolo[0].valoreCarta2;
        scala[4] = tavolo[0].valoreCarta3;
        scala[5] = tavolo[0].valoreCarta4;
        scala[6] = tavolo[0].valoreCarta5;
        
        // ordino i valori
        mergeSort(scala,7);
        
    }
    
    return ;
}

void vincitore(int numGiocatori, struct giocatore infoGiocatore[], struct carteComuni tavolo[]){
    
    return ;
}

double input(double min, double max){
    double valore;
    
    do{
        scanf("%lf",&valore);
        if(valore < min || valore > max){
            printf("\nIl valore deve essere compreso tra %.0lf e %.0lf!\nRiprova: ",min,max);
        }
    }while(valore < min || valore > max);
    
    return valore;
}

int realRand(int min, int max){
    double range = (max - min); 
    double divisore = RAND_MAX / range;
    return (int)(min + (lrand48() / divisore));
}

int **creaMatrice(int righe, int colonne){
    int i;
    
    int **M = (int **) malloc(righe * sizeof(int *));
    if(M == NULL){
        perror("\nError");
        printf("\n");
        exit(2);
    }
    
    for(i=0; i<righe; i++){
        M[i] = (int *) malloc(colonne * sizeof(int));
        if(M == NULL){
            perror("\nError");
            printf("\n");
            exit(3);
        }
    }
    
    return M;
}

void initMatrice(int righe, int colonne, int **M, int valore){
    int i, j;
    
    for(i=0; i<righe; i++)
        for(j=0; j<colonne; j++)
            M[i][j] = valore;
            
    return ;
}

void liberaMatrice(int righe, int **M){

    while(--righe > -1){
        free(M[righe]);
    }
    
    free(M);
    
    return ;
}

// Function to merge Arrays L and R into A.
void merge(int *A, int *L, int leftCount, int *R, int rightCount){
    // lefCount = number of elements in L
    // rightCount = number of elements in R.
    // i - to mark the index of left aubarray (L)
    // j - to mark the index of right sub-raay (R)
    // k - to mark the index of merged subarray (A)
    int i=0, j=0, k=0;

    while(i<leftCount && j< rightCount){
        if(L[i] < R[j])
            A[k++] = L[i++];
        else
            A[k++] = R[j++];
    }
	
    while(i < leftCount)
        A[k++] = L[i++];
    while(j < rightCount)
        A[k++] = R[j++];
        
    return ;
}

// Recursive function to sort an array of integers.
void mergeSort(int *A, int n){
    int mid, i, *L, *R;
    
    if(n < 2)
        return;             // base condition. If the array has less than two element, do nothing. 
    
    mid = n/2;              // find the mid index. 
    
    // create left and right subarrays
    // mid elements (from index 0 till mid-1) should be part of left sub-array 
    // and (n-mid) elements (from mid to n-1) will be part of right sub-array
    L = (int*)malloc(mid * sizeof(int)); 
    R = (int*)malloc((n - mid) * sizeof(int)); 
    
	for(i=0; i<mid; i++)
	    L[i] = A[i];        // creating left subarray
	for(i=mid; i<n; i++)
	    R[i-mid] = A[i];    // creating right subarray

    mergeSort(L,mid);       // sorting the left subarray
    mergeSort(R,n-mid);     // sorting the right subarray
    merge(A,L,mid,R,n-mid); // Merging L and R into A as sorted list.
	
    free(L);
    free(R);
}
/************************************* FUNZIONI **************************************/

